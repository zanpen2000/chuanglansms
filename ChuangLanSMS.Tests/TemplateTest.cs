// <copyright file="TemplateTest.cs">Copyright ©  2016</copyright>
using System;
using ChuangLanSMS;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ChuangLanSMS.Tests
{
    /// <summary>This class contains parameterized unit tests for Template</summary>
    [PexClass(typeof(Template))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class TemplateTest
    {
        /// <summary>Test stub for Render(Int32, String[])</summary>
        [PexMethod]
        public string RenderTest(int template_id, string[] args)
        {
            string result = Template.Render(template_id, args);
            return result;
            // TODO: add assertions to method TemplateTest.RenderTest(Int32, String[])



        }


        
    }
}
