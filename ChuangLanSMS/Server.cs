﻿using System;
using System.Diagnostics;
using System.Net;
using System.Configuration;

/// <summary>
/// 创蓝短信平台接口封装 
/// 
/// 回馈服务器内置用户 admin, 密码： 123456
/// 
/// 网址：http://222.73.117.158 测试用户:jiekou-dx-01 测试密码：Tcl888888
/// 
/// 状态报告回送地址  http://xxx.xxx.xxx.xxx:6001/chuanglansms/status_report （已失效）
/// 短信回送地址 http://xxx.xxx.xxx.xxx:6001/chuanglansms/pushMo （已失效）
/// 
/// 测试网站架设在 xxx.xxx.xxx.xxx 网站端口可以由公司临时开通映射
/// 
/// 
/// 
/// </summary>
namespace ChuangLanSMS
{

    using Logger;

    public class Server
    {
        static string chuanglan_user  = ConfigurationManager.AppSettings["cl_user"];
        static string chuanglan_pwd = ConfigurationManager.AppSettings["cl_pwd"];

        static string SEND_URL = ConfigurationManager.AppSettings["SendUrl"];
        static string BALANCE_QUERY_URL = ConfigurationManager.AppSettings["BalanceUrl"];
        static string SEND_BATCH_URL = SEND_URL;

        static string inUser = ConfigurationManager.AppSettings["LocalUser"];
        static string inPwd = ConfigurationManager.AppSettings["LocalPwd"];

        /// <summary>
        /// 短信发送（失效）
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <param name="mobile"></param>
        /// <param name="content"></param>
        /// <param name="needstatus"></param>
        /// <param name="product"></param>
        /// <param name="extno"></param>
        /// <returns></returns>
        [Obsolete]
        public static string send(
            string account,
            string pwd,
            string mobile,
            string content,
            bool needstatus,
            string product,
            string extno)
        {

            var query_string = string.Format("?account={0}&pswd={1}&mobile={2}&msg={3}&needstatus={4}", account, pwd, mobile, content, needstatus);

            if (!string.IsNullOrEmpty(product))
            {
                query_string += string.Format("&product={0}", product);
            }

            if (!string.IsNullOrEmpty(extno))
            {
                query_string += string.Format("&extno={0}", extno);
            }

            using (WebClient client = new WebClient())
            {
                Log.AppendUserMessage(SEND_URL + query_string);
                Debug.Print(SEND_URL + query_string);
                return client.DownloadString(SEND_URL + query_string);
            }
        }

        /// <summary>
        /// 短信发送（批量）
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <param name="mobiles"></param>
        /// <param name="content"></param>
        /// <param name="needstatus"></param>
        /// <param name="product"></param>
        /// <param name="extno"></param>
        /// <returns>0表示成功其他代码参见网站 http://www.cl2009.com/ndxjk/returncode.html </returns>
        public static string batchSend(
            string account,
            string pwd,
            string mobiles,
            string content,
            bool needstatus,
            string product,
            string extno
            )
        {
            var query_string = string.Format("?account={0}&pswd={1}&mobile={2}&msg={3}&needstatus={4}", account, pwd, mobiles, content, needstatus);

            if (!string.IsNullOrEmpty(product))
            {
                query_string += string.Format("&product={0}", product);
            }

            if (!string.IsNullOrEmpty(extno))
            {
                query_string += string.Format("&extno={0}", extno);
            }

            using (WebClient client = new WebClient())
            {
                Log.AppendUserMessage(SEND_BATCH_URL + query_string);
                return client.DownloadString(SEND_BATCH_URL + query_string);
            }
        }

        /// <summary>
        /// 状态报告推送
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <param name="msgId"></param>
        /// <param name="reportTime"></param>
        /// <param name="mobile"></param>
        /// <param name="status"></param>
        public static void statusReport(
            string account, 
            string pwd, 
            string msgId,
            string reportTime,
            string mobile,
            string status
            ) {


            if (account.Trim() != inUser || pwd.Trim() != inPwd) {
                Log.AppendErrorInfo("用户名和密码不正确", new AccountExecption());
                return;
            }

            Log.AppendInfo(
                string.Format("MessageId:{0}, ReportTime:{1}, Mobile:{2}, Status:{3}", msgId, reportTime, mobile, status)
                );
        }


        /// <summary>
        /// 短信回送
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <param name="moTime"></param>
        /// <param name="mobile"></param>
        /// <param name="destCode"></param>
        /// <param name="msg"></param>
        /// <param name="msgDestCode"></param>
        public static void pushMo(
            string account,
            string pwd,
            string moTime,
            string mobile,
            string destCode,
            string msg,
            string isems,
            string emshead
            ) {

            if (account.Trim() != inUser || pwd.Trim() != inPwd)
            {
                Log.AppendErrorInfo("用户名和密码不正确", new AccountExecption());
                return;
            }

            Log.AppendInfo(
                string.Format("接收时间:{0}, 手机号:{1}, 描述码:{2}, 短信内容:{3}, 短信描述码:{4}", moTime, mobile, destCode, msg, isems)
                );

        }

        /// <summary>
        /// 额度查询
        /// </summary>
        /// <returns></returns>
        public static string queryBalance(string account, string pwd)
        {

            string args = string.Format("account={0}&pswd={1}", account, pwd);
            string result;
            using (WebClient client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "aplication/x-www-form-urlencoded";
                result = client.UploadString(BALANCE_QUERY_URL, args);
            }

            return result;
        }
    }

    /// <summary>
    /// 账号异常类，默认tsdykj,dykjhz123456
    /// </summary>
    public class AccountExecption : Exception { }
}