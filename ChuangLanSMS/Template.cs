﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ChuangLanSMS
{
    public class Template
    {
        public static string Template1 = "亲爱的用户,您的XXX已超期,请XXXX.XX码:{0}. 服务热线:4008000000";
        public static string Template2 = "由{0}派送的快件已送至{1},XX码:{2}. 服务热线:400808000000";
        public static string Template3 = "转发信息：您的取件密码是{0}，请根据您在快递信息填写的手机号和密码到相应快递柜取件。本条信息无需回复。";

        /// <summary>
        /// 使用：
        /// Render(1, ("123456"))
        /// 模板说明:
        /// Template1 -> "亲爱的用户,您的快件已超期,请及时收取.取件码:{0}. 服务热线:4008086006";
        /// Template2 -> "由{0}派送的快件已送至{1},取件码:{2}. 服务热线:4008086006";
        /// Template3 -> "转发信息：您的取件密码是{0}，请根据您在快递信息填写的手机号和密码到相应快递柜取件。本条信息无需回复。";
        /// </summary>
        /// <param name="template_id">模板id</param>
        /// <param name="args">字符串参数数组</param>
        /// <returns></returns>
        public static string Render(int template_id, params string[] args)
        {
            if (template_id < 1 || template_id > 3)
                throw new ArgumentOutOfRangeException("模板编码超出范围");

            if (args == null || args.Length ==0)
                throw new ArgumentNullException("参数数组不能为空");

            if (args.Any(s => string.IsNullOrEmpty(s)))
                throw new ArgumentException("数组内值不能为空");

            string result = "";
            switch (template_id)
            {
                case 1:
                    result = string.Format(Template1, args);
                    break;
                case 2:
                    result = string.Format(Template2, args);
                    break;
                case 3:
                    result = string.Format(Template3, args);
                    break;
            }

            return result;
        }
    }
}
