﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChuangLanSMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChuangLanSMS.Tests
{
    [TestClass()]
    public class TemplateTests
    {
        [TestMethod()]
        public void RenderTestWithStringValue()
        {
            string result = Template.Render(1, "123123");
            Assert.AreEqual(string.Format(Template.Template1, "123123"), result);

            result = Template.Render(2, "张三", "何当金络脑，快马踏清秋","1234");
            Assert.AreEqual(string.Format(Template.Template2, "张三", "上面那句好屌哦", "1234"), result);

            result = Template.Render(3, "1234");
            Assert.AreEqual(string.Format(Template.Template3, "1234"), result);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RenderTestWithException()
        {
            Template.Render(1,null);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RenderOutOfRangeTest()
        {
            Template.Render(0, "123");
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void RenderEmptyStringValueTest()
        {
            Template.Render(1,"");
        }
    }
}