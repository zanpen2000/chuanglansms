﻿using ChuangLanSMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace cl_sms.Controllers
{
    public class ChuangLanSMSController: Controller
    {

        [HttpPost]
        public ActionResult send() {

            var account = Request.QueryString["account"];
            var pwd = Request.QueryString["pwd"];
            var mobiles = Request.QueryString["mobiles"];
            var msg = Request.QueryString["msg"];
            bool needstatus = Convert.ToBoolean(Request.QueryString["needstatus"]);
            var extno = Request.QueryString["extno"];
            var product = Request.QueryString["product"];

            var result = ChuangLanSMS.Server.batchSend(account, pwd, mobiles, msg, needstatus, product, extno);

            return Content(result);
        }

        [HttpPost]
        public ActionResult queryBalance()
        {
            var account = Request.QueryString["account"];
            var pwd = Request.QueryString["pwd"];

            var result = ChuangLanSMS.Server.queryBalance(account, pwd);

            return Content(result);
        }

        public ActionResult status_report() {
            string account = Request.QueryString["receiver"];
            string pwd = Request.QueryString["pswd"];
            string msgId = Request.QueryString["msgid"];
            string reportTime = Request.QueryString["reportTime"];
            string mobile = Request.QueryString["mobile"];
            string status = Request.QueryString["status"];

            ChuangLanSMS.Server.statusReport(account, pwd, msgId, reportTime, mobile, status);
            return Content("");
        }

        public ActionResult pushMo()
        {
            string account = Request.QueryString["receiver"];
            string pwd = Request.QueryString["pswd"];
            string moTime = Request.QueryString["moTime"];
            string mobile = Request.QueryString["mobile"];
            string destCode = Request.QueryString["destcode"];
            string msg = Request.QueryString["msg"];
            string isems = Request.QueryString["isems"];
            string emshead = Request.QueryString["emshead"];

            ChuangLanSMS.Server.pushMo(account, pwd, moTime, mobile, destCode, msg, isems, emshead);
            return Content("");
        }
    }
}